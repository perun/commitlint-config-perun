## [1.0.1](https://gitlab.ics.muni.cz/perun/commitlint-config-perun/compare/v1.0.0...v1.0.1) (2024-03-15)


### Bug Fixes

* **deps:** update dependency @commitlint/config-conventional to v19 ([aecfe36](https://gitlab.ics.muni.cz/perun/commitlint-config-perun/commit/aecfe362e62a516434f0c8bb6e82e5d23d7a08ab))

# 1.0.0 (2024-01-17)


### Features

* initial commit ([4ffc702](https://gitlab.ics.muni.cz/perun/commitlint-config-perun/commit/4ffc7026d247b05a87eaf7a720e859b1143ce52d))
