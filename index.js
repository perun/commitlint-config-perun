module.exports = {
  extends: ["@commitlint/config-conventional"],
  helpUrl:
    "https://gitlab.ics.muni.cz/perun/common/-/blob/main/CONTRIBUTING.md",
  rules: {
    "scope-enum": [2, "always", ["release", "deps"]],
    "body-max-line-length": [2, "always", 200],
  },
  prompt: {
    questions: {
      isIssueAffected: {
        description: "Are there any specifics for deployment (upgrade notes)?",
      },
      issuesBody: {
        description:
          "If there are deployment/upgrade notes, the commit requires a body. Please enter a longer description of the commit itself",
      },
      issues: {
        description:
          "Enter DEPLOYMENT NOTE: description of what needs to be done",
      },
    },
  },
};
